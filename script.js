function timeOutCtrl($scope,$timeout) {
    $scope.onTimeout = function () {
        $scope.today = new Date();
        $scope.tomorrow = new Date();
        $scope.tomorrow.setDate($scope.tomorrow.getDate() + 1);
        $scope.tomorrow.setHours(0);
        $scope.tomorrow.setMinutes(0);
        $scope.tomorrow.setSeconds(0);
        $scope.diff = Math.floor(Math.abs(new Date($scope.tomorrow) - $scope.today) / 1000);
        $scope.seconds = pad(($scope.diff % 3600) % 60);
        $scope.minutes = pad(Math.floor($scope.diff / 60) % 60);
        $scope.hours = pad(Math.floor($scope.diff / 3600) % 24);
        mytimeout = $timeout($scope.onTimeout, 1000);
    }
    var mytimeout = $timeout($scope.onTimeout,1000);
}

function pad(num)
{
    if(num < 10)
    {
        num = '0' + num;
    }
    return num;
}

function changeOpacity(diff)
{
    
}